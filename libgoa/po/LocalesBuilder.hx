package libgoa.po;

import haxe.macro.Context;
import haxe.macro.Expr;
import libgoa.AE;
using StringTools;

class LocalesBuilder {

    static public function build() {
        var fields = Context.getBuildFields();
        
        var localesDir = Context.definedValue("localesPath");
        if( localesDir == null ) localesDir = "locales/";
        var localesPath = resolvePath(localesDir);

        if (!sys.FileSystem.exists(localesPath) || !sys.FileSystem.isDirectory(localesPath)) {
            Context.warning('$localesPath must be a directory.', Context.currentPos());
            return fields;
        }
        
        var readed = Itzul.readPoFiles(localesPath);
        var r, locText, expr, exprInd, field, fget, pos = Context.currentPos();
        var createdFields = new Map<String, Int>();
        
        for (loc in readed.keys()) {
            r = readed.get(loc);
            fields.push({
                name: 'loc_$loc',
                pos: pos,
                meta: [{ name : ":dce", params : [], pos : pos }],
                kind: FVar(macro :String, macro $v{loc}),
                access: [AStatic, APublic, AInline],
            });

            for (locId in r.keys()) {
                locText = r.get(locId);
                // without this if, we would repeat the variable name for every locale
                if (!createdFields.exists(locId)) {
                    fget = {
                        name: 'get_' + locId,
                        pos: pos,
                        meta: [{ name : ":dce", params : [], pos : pos }],
                        kind: FFun({
                            args: [],
                            params: [],
                            ret: macro :String,
                            expr: {expr: EReturn(macro libgoa.po.Itzul.getLocalized($v{locId})), pos:pos}
                        }),
                        access: [AStatic, AInline, APrivate]
                    };
                    field = {
                        name: locId,
                        pos: pos,
                        meta: [{ name : ":dce", params : [], pos : pos }],
                        kind: FProp('get', 'never', macro :String),
                        doc: '${loc}: ${locText}',
                        access: [AStatic, APublic]
                    };
                    var fieldInd = fields.push(field);
                    fields.push(fget);
                    createdFields.set(locId, fieldInd-1);
                } else {
                    // if there was an existing field for this localizable_id
                    // add the current translation to the documentation of that field
                    exprInd = createdFields.get(locId);
                    expr = fields[exprInd];
                    expr.doc += '\n${loc}: ${locText}';
                    fields[exprInd] = expr;
                }
            }
        }
        
        return fields;
    }
    
    static function resolvePath(dir:String) {
		var pos = Context.currentPos();
		dir = try Context.resolvePath(dir) 
                catch( e : Dynamic ) { 
                    Context.warning("Localizations directory not found in classpath '" + dir + "' (use -D localesPath=DIR)", pos);
                    return "__invalid";
                }
        var path = '';
        #if (sys || nodejs)
		path = sys.FileSystem.fullPath(dir);
		if( !sys.FileSystem.exists(path) || !sys.FileSystem.isDirectory(path) )
			Context.warning("Localizations directory does not exists '" + path + "'", pos);
        #end
		return path + '/';
    }
}