package libgoa.po;

#if !macro
@:build(libgoa.po.LocalesBuilder.build())
#end
class Itzul {
    
    // Map<locale_code, Map<localizableId, localizedText>>
    static public var strings:Map<String, Map<String, String>>;
    static public var currentLocale:String;
    static public var locales:Array<String>;
    
    static public function setLocale(_newLoc:String) currentLocale = _newLoc;
    
    static public function readPoFiles(localesRelDir:String, locale:String = '') {
        currentLocale = '';
        locales = [];
        
        #if (sys || nodejs)
        try {
            #if (macro && haxe_ver >= 4.0)
    		var exePath = null;
    		#else
            var pr = Sys.programPath();
            var exePath = pr == null ? null : pr.split("\\").join("/").split("/");
            if( exePath != null ) exePath.pop();
            #end
            var froot = exePath == null ? localesRelDir : sys.FileSystem.fullPath(exePath.join("/") + "/" + localesRelDir);
            if( froot == null || !sys.FileSystem.exists(froot) || !sys.FileSystem.isDirectory(froot) ) {
                froot = sys.FileSystem.fullPath(localesRelDir);
                if( froot == null || !sys.FileSystem.exists(froot) || !sys.FileSystem.isDirectory(froot) )
                    throw "Could not find dir " + localesRelDir;
            }
            var localesDir = froot.split("\\").join("/");
            if (!StringTools.endsWith(localesDir, '/')) localesDir += '/';
            for( poFile in sys.FileSystem.readDirectory(localesDir) ) {
                processPOContent(StringTools.replace(poFile, '.po', ''), sys.io.File.getContent(localesDir + poFile), locale);
            }

        } catch (e:Any) {
            trace('error reading po files from $localesRelDir: $e');
        }
        #end
        
        return strings;
    }

    static public function processPOContent(locName:String, content:String, locale:String = '') {

        if (strings == null)
            strings = new Map();
        if (locales == null)
            locales = [];
        
        var locId, locMsg, escapeChar;
        var escapeEreg = ~/\\(n|t)/g;
        
        locales.push(locName);
        if (locale != '' && locale != locName ) return;
        if (currentLocale == '') currentLocale = locName;
        var locStrings = new Map();
        
        while (AE.PO_MSG.match(content)) {
            locId = AE.PO_MSG.matched(1);
            locMsg = AE.PO_MSG.matched(2);
            if (escapeEreg.match(locMsg)) {
                escapeChar = switch escapeEreg.matched(1) {
                                    case 'n': '\n';
                                    case 't': '\t';
                                    case _: '';
                                }
                locMsg = escapeEreg.replace(locMsg, escapeChar);
            }
            locStrings.set(locId, locMsg);
            content = AE.PO_MSG.matchedRight();
        }

        strings.set(locName, locStrings);
    }

    static macro public function embed() {
        return macro @:privateAccess {
                libgoa.po.Itzul.strings = libgoa.po.EmbedLocales.embed();
                if (libgoa.po.Itzul.strings != null) {
                    var keys = libgoa.po.Itzul.strings.keys();
                    libgoa.po.Itzul.currentLocale = (keys.hasNext()) ? keys.next() : '';
                }
            }
    }

    static public function getLocalized(id:String, defLoc:String = null) : String {

        var localizedText = id;
        localizedText = getLocalizedForLocale(id, Itzul.currentLocale);
        if (localizedText == id && defLoc != null)
            localizedText = getLocalizedForLocale(id, defLoc);
        
        return localizedText;
    }

    @:noCompletion static public function getLocalizedForLocale(id:String, loc:String) {
        if (Itzul.strings == null || id == null || loc == null)
            return (id == null) ? '__notfound__' : id;
        
        var _retText = null;
        var _locMap = Itzul.strings.get(loc);
        if (_locMap != null)
            _retText = _locMap.get(id);
        return (_retText != null) ? _retText : id;
    }
}