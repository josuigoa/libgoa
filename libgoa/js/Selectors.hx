package libgoa.js;

#if hx3compat
import js.jquery.Helper.J;
#end

typedef Selectors = {}

@:build(libgoa.js.SelectorBuilder.buildId())
class Id{}

@:build(libgoa.js.SelectorBuilder.buildCls())
class Cls{}

@:build(libgoa.js.SelectorBuilder.buildTag())
class Tag{}

abstract IdSel(String) to String from String {
    inline public function new(id) this = id;
    /**
     *  @return '#' + this
     */
    inline public function selector() return '#' + this;
    /**
     *  @return js.Browser.document.getElementById(this)
     */
    inline public function dom() return js.Browser.document.getElementById(this);
    #if hx3compat
    /**
     *  @return jQuery('#' + this)
     */
    inline public function jq() return J(selector());
    #end
}
abstract ClsSel(String) to String from String {
    inline public function new(cls) this = cls;
    /**
     *  @return '.' + this
     */
    inline public function selector() return '.' + this;
    /**
     *  @return js.Browser.document.getElementsByClassName(this)
     */
    inline public function dom() return js.Browser.document.getElementsByClassName(this);
    #if hx3compat
    /**
     *  @return jQuery('.' + this)
     */
    inline public function jq() return J(selector());
    /**
     *  @return jQuery(this + ' ' + extraSelector)
     */
    inline public function specify(extraSelector:String) return J(selector() + ' $extraSelector');
    #end
}
abstract TagSel(String) to String from String {
    inline public function new(tag) this = tag;
    /**
     *  @return this
     */
    inline public function selector() return this;
    /**
     *  @return js.Browser.document.getElementsByTagName(this)
     */
    inline public function dom() return js.Browser.document.getElementsByTagName(this);
    #if hx3compat
    /**
     *  @return jQuery(this)
     */
    inline public function jq() return J(selector());
    /**
     *  @return jQuery(this + '[for="' + id + '"]')
     */
    inline public function forId(id:String) return J(selector() + '[for="$id"]');
    /**
     *  @return jQuery(this + '[type="' + id + '"]')
     */
    inline public function ofType(type:String) return J(selector() + '[type="$type"]');
    /**
     *  @return jQuery(this + ' ' + extraSelector)
     */
    inline public function specify(extraSelector:String) return J(selector() + ' $extraSelector');
    #end
}