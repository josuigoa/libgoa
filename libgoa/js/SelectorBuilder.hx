package libgoa.js;

import haxe.macro.Context;
import haxe.macro.Expr;
import libgoa.AE;
using StringTools;

enum SelType {
    id;
    cls;
    tag;
}

class SelectorBuilder {

    static public function buildId()
        return build(AE.ID_ATTR, id);
    static public function buildCls()
        return build(AE.CLASS_ATTR, cls);
    static public function buildTag()
        return build(AE.TAG, tag);

    static public function build(ereg:EReg, type:SelType):Array<Field> {
        var defPath = Context.definedValue("indexPath");
        var paths = (defPath == null) ? ['index.html'] : defPath.split(',');
        var extra = null;
        var extraDefName = switch type {
                                case id: 'extraId';
                                case cls: 'extraCls';
                                case tag: 'extraTag';
                            }
        var extraString = Context.definedValue(extraDefName);
        if( extraString != null ) 
            extra = [for (e in extraString.split(',')) if (e.trim() != '') e.trim()];
        
        var fileContent, elements = new Map<String, Elem>(), el, clsElem;
        for (p in paths) {
            fileContent = getFileContent(p);
            while( ereg.match(fileContent) ) {
                el = ereg.matched(1);
                if (type == cls) {
                    clsElem = el.split(' ');
                    for (ce in clsElem) {
                        elements.remove(ce);
                        elements.set(ce, new Elem(ce, p));
                    }
                } else {
                    elements.remove(el);
                    elements.set(el, new Elem(el, p));
                }
                fileContent = ereg.matchedRight();
            }
            if (extra != null) {
                for (e in extra) {
                    if (StringTools.trim(e) == '') continue;
                    elements.remove(e);
                    elements.set(e, new Elem(e, p));
                }
            }
        }
        return Context.getBuildFields().concat(buildFields(elements, type));
    }
    
    static function buildFields(_fieldNames:Map<String, Elem>, type:SelType):Array<Field> {
        var fields = [];
        var pos = Context.currentPos();
        var varComplexType = switch type {
                                case id: macro :libgoa.js.Selectors.IdSel;
                                case cls: macro :libgoa.js.Selectors.ClsSel;
                                case tag: macro :libgoa.js.Selectors.TagSel;
                            }
        for (f in _fieldNames) {
            fields.push({
                name : AE.KARAK_BALIOGABEAK.replace(f.name, '_'),
                access : [APublic, AStatic, AInline],
                meta : [{ name : ':dce', params : [], pos : pos}],
                pos : pos,
                doc : '"${f.name}" selector from "${f.origin}" file.',
                kind : FVar(varComplexType, macro $v{f.name})
            });
        }
        return fields;
    }
    
    static function getFileContent(_path:String) {
        try {
            var p = Context.resolvePath(_path);
            return sys.io.File.getContent(p);
        } catch(e:Dynamic) {
            Context.error('$_path fitxategia kargatzean errorea: $e', Context.currentPos());
            return '';
        }
    }
}

private class Elem {
    public var name:String;
    public var origin:String;
    public function new(n, o) {
        name = n;
        origin = o;
    }
}