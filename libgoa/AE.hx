package libgoa;

class AE {
	static public var LERRO_SALTOA = ~/\r?\n/g;
	static public var ZURIUNEA = ~/[\t|\s]+/g;
	static public var KARAK_BALIOGABEAK = ~/[^A-Za-z0-9_]/g;
	static public var DATA_EU = ~/([0-9]{4,4})\/([0-9]{1,2})\/([0-9]{1,2})/g;
	static public var DATA = ~/([0-9]{1,2})\/([0-9]{1,2})\/([0-9]{4,4})/g;
	static public var ORDUA = ~/([0-9]{1,2}):([0-9]{2})(?::([0-9]{2}))?/g;
	static public var ID_ATTR = ~/id=["']([A-Za-z0-9_-]+)["']/;
	static public var CLASS_ATTR = ~/class=["']([A-Za-z0-9_\s-]+)["']/;
	static public var TAG = ~/<([A-Za-z0-9]+)/;
	static public var PO_MSG = ~/msgid ['"](.+)['"]\n+msgstr ['"](.+)['"]/g;
}