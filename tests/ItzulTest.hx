package;

import tink.testrunner.Assertion;
import tink.unit.Assert.assert;
import tink.unit.AssertionBuffer;
import libgoa.po.Itzul;

using tink.CoreApi;

class ItzulTest  {

    public function new() {}
    
    public function loadedCount() {
        return assert(Itzul.locales.length == 4);
    }
    
    @:setup
    public function readPoFiles() {
       Itzul.readPoFiles('./tests/data/locales/');
       return Noise;
    }
    
    @:variant('en_GB')
    @:variant('es_ES')
    @:variant('eu_ES')
    @:variant('fr_FR')
    public function setLocale(loc:String) {
        Itzul.setLocale(loc);
        return assert(Itzul.currentLocale == loc);
    }
    
    @:variant('en_GB', 'Sadkoren itzala', "Sadko's shadow")
    @:variant('es_ES', 'Sadkoren itzala', 'La sombra de Sadko')
    @:variant('eu_ES', 'Sadkoren itzala', 'Sadkoren itzala')
    @:variant('fr_FR', 'Sadkoren itzala', "L'ombre de Sadko")
    public function getId(loc:String, id:String, str:String) {
        
        var buff = new AssertionBuffer();
        
        Itzul.setLocale(loc);
        buff.assert(Itzul.getLocalized(id) == str);
        
        Itzul.setLocale('false_locale');
        buff.assert(Itzul.getLocalized(id, loc) == str);
        buff.assert(Itzul.getLocalized('notExistingId', loc) == 'notExistingId');
        buff.assert(Itzul.getLocalized(null, loc) == '__notfound__');
        
        // Itzul.setLocale('false_locale');
        buff.assert(Itzul.getLocalized(id) == id);
        
        return buff.done();
    }
}