package;

import tink.testrunner.Assertion;
import tink.unit.Assert.assert;
import tink.unit.AssertionBuffer;
import libgoa.Agian;

class AgianTest {

    public function new() {}
    
    public function basic() {
        var value:Agian<Int> = null;
        
        var buff = new AssertionBuffer();
        
        buff.assert(!value.exists());
        buff.assert(value.or(0) == 0);
        var message = value.mapDefault(function(value) return "Value is " + value, "No value");
        buff.assert(message == 'No value');
        
        var sureBool;
        try {
            value.sure();
            sureBool = false;
        } catch (e:Dynamic) {
            sureBool = true;
        }
        buff.assert(sureBool, 'value.sure() throws an exception');
        
        value.or(20);
        buff.assert(!value.exists());
        value.orSet(20);
        buff.assert(value == 20);
        
        value = 10;
        buff.assert(value.or(0) == 10);
        
        var valueString = value.map(function(value) return Std.string(value));
        buff.assert(valueString == '10');

        var message = value.mapDefault(function(value) return "Value is " + value, "No value");
        buff.assert(message == 'Value is 10');
        
        return buff.done();
    }
}