package;

import tink.testrunner.Runner;
import tink.unit.TestBatch;

using tink.CoreApi;

class RunTests {

    static function main() {
        
        Runner.run(TestBatch.make([
            new ItzulTest(),
            new AETest(),
            new SelectorTest(),
            new AgianTest()
        ])).handle(Runner.exit);
    }
}