package;

import tink.testrunner.Assertion;
import tink.unit.Assert.assert;
import tink.unit.AssertionBuffer;
import libgoa.AE;

class AETest {
    
    public function new() {}
    
    function whileEregCount(ereg:EReg, str) {
        var kont = 0;
        while (ereg.match(str)) {
            kont++;
            str = ereg.matchedRight();
        }
        return kont;
    }
    
    public function lerroSaltoCount() {
        var str = 'hau\nda\r\nlerro\nprobatako\r\nsalto\r\nkopuru\r\nhaundi\nbat';
        return assert(whileEregCount(AE.LERRO_SALTOA, str) == 7);
    }
    
    public function zuriuneakCount() {
        var str = 'bat bi hiru lau bortz sei zazpi zortzi bedetzi hamar ';
        return assert(whileEregCount(AE.ZURIUNEA, str) == 10);
    }
    
    public function karakBaliogabeakCount() {
        var buff = new AssertionBuffer();
        
        var str = 'hau da-karaktere_baliogabea~eta_identifikatu_behar_luke/';
        buff.assert(AE.KARAK_BALIOGABEAK.match(str));
        buff.assert(AE.KARAK_BALIOGABEAK.matched(0) == ' ');
        
        str = AE.KARAK_BALIOGABEAK.matchedRight();
        buff.assert(AE.KARAK_BALIOGABEAK.match(str));
        buff.assert(AE.KARAK_BALIOGABEAK.matched(0) == '-');
        
        str = AE.KARAK_BALIOGABEAK.matchedRight();
        buff.assert(AE.KARAK_BALIOGABEAK.match(str));
        buff.assert(AE.KARAK_BALIOGABEAK.matched(0) == '~');
        
        str = AE.KARAK_BALIOGABEAK.matchedRight();
        buff.assert(AE.KARAK_BALIOGABEAK.match(str));
        buff.assert(AE.KARAK_BALIOGABEAK.matched(0) == '/');
        
        str = AE.KARAK_BALIOGABEAK.matchedRight();
        buff.assert(AE.KARAK_BALIOGABEAK.match(str) == false);
        
        return buff.done();
    }
    
    public function data() {
        var buff = new AssertionBuffer();
        
        buff.assert(AE.DATA.match('21/8/1985'));
        buff.assert(AE.DATA.matched(0) == '21/8/1985');
        buff.assert(AE.DATA.matched(1) == '21');
        buff.assert(AE.DATA.matched(2) == '8');
        buff.assert(AE.DATA.matched(3) == '1985');
        
        buff.assert(AE.DATA.match('1985/8/21') == false);
        buff.assert(AE.DATA.match('14/3/1') == false);
        buff.assert(AE.DATA.match('16/09/01') == false);
        buff.assert(AE.DATA.match('1958/08/21') == false);
        
        return buff.done();
    }
    
    public function dataEU() {
        var buff = new AssertionBuffer();
        
        buff.assert(AE.DATA_EU.match('1985/8/21'));
        buff.assert(AE.DATA_EU.matched(0) == '1985/8/21');
        buff.assert(AE.DATA_EU.matched(1) == '1985');
        buff.assert(AE.DATA_EU.matched(2) == '8');
        buff.assert(AE.DATA_EU.matched(3) == '21');
        
        
        return buff.done();
    }
    
    public function ordua() {
        var buff = new AssertionBuffer();
        
        buff.assert(AE.ORDUA.match('3:27:19'));
        buff.assert(AE.ORDUA.matched(0) == '3:27:19');
        buff.assert(AE.ORDUA.matched(1) == '3');
        buff.assert(AE.ORDUA.matched(2) == '27');
        buff.assert(AE.ORDUA.matched(3) == '19');
        
        buff.assert(AE.ORDUA.match('16:22'));
        buff.assert(AE.ORDUA.matched(0) == '16:22');
        buff.assert(AE.ORDUA.matched(1) == '16');
        buff.assert(AE.ORDUA.matched(2) == '22');
        
        buff.assert(AE.ORDUA.match('10:32:8'));
        buff.assert(AE.ORDUA.matched(0) == '10:32');
        buff.assert(AE.ORDUA.matched(1) == '10');
        buff.assert(AE.ORDUA.matched(2) == '32');
        buff.assert(AE.ORDUA.matched(3) == null);
        
        buff.assert(AE.ORDUA.match('10:9') == false);
        buff.assert(AE.ORDUA.match('11:2') == false);
        buff.assert(AE.ORDUA.match('10.32:2') == false);
        
        return buff.done();
    }
    
    public function id_attr() {
        var buff = new AssertionBuffer();
        
        buff.assert(AE.ID_ATTR.match("id='proba'"));
        buff.assert(AE.ID_ATTR.matched(1) == 'proba');
        
        buff.assert(AE.ID_ATTR.match('id="proba"'));
        buff.assert(AE.ID_ATTR.matched(1) == 'proba');
        
        buff.assert(AE.ID_ATTR.match("id='proba_01'"));
        buff.assert(AE.ID_ATTR.matched(1) == 'proba_01');
        
        buff.assert(AE.ID_ATTR.match("id='proba-01'"));
        buff.assert(AE.ID_ATTR.matched(1) == 'proba-01');
        
        buff.assert(AE.ID_ATTR.match('id=proba') == false);
        buff.assert(AE.ID_ATTR.match('id="proba 01"') == false);
        buff.assert(AE.ID_ATTR.match('class="proba 01"') == false);
        
        return buff.done();
    }
    
    public function class_attr() {
        var buff = new AssertionBuffer();
        
        buff.assert(AE.CLASS_ATTR.match("class='proba ezkutua'"));
        buff.assert(AE.CLASS_ATTR.matched(1) == 'proba ezkutua');
        
        buff.assert(AE.CLASS_ATTR.match('class="proba"'));
        buff.assert(AE.CLASS_ATTR.matched(1) == 'proba');
        
        buff.assert(AE.CLASS_ATTR.match("class='probatarako_da klase-multzo hau'"));
        buff.assert(AE.CLASS_ATTR.matched(1) == 'probatarako_da klase-multzo hau');
        
        buff.assert(AE.CLASS_ATTR.match('class=proba') == false);
        buff.assert(AE.CLASS_ATTR.match('id="proba 01"') == false);
        
        return buff.done();
    }
    
    public function tag() {
        var buff = new AssertionBuffer();
        
        buff.assert(AE.TAG.match('<h1>'));
        buff.assert(AE.TAG.matched(1) == 'h1');
        
        buff.assert(AE.TAG.match('<h1 class="titulua">'));
        buff.assert(AE.TAG.matched(1) == 'h1');
        
        buff.assert(AE.TAG.match('< h1>') == false);
        
        return buff.done();
    }
    
    // PO_MSG
}